// segment.cpp
#include "segment.H"

segment :: segment(scalar len, label idx)
{
	length = len;
	cellIdx = idx;
}

scalar segment :: getLength()
{
	return length;
}

label segment :: getIdx()
{
	return cellIdx;
}


