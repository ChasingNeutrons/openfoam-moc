//Ray class - contains start, end, and segment length data

class rayDynamic {

	private:

	//Ray identifier
	label rayID;

	//Number of segments
	label segments;

	//List of segment lengths
	DynamicList<scalar> segmentLengths;

	//List of cell IDs
	DynamicList<label> cellIndices;

	//List of x co-ords for ray-cell intersection point
	//First index corresponds to incoming point, second to outgoing
	DynamicList<scalar> xInF;
	DynamicList<scalar> xInB;

	//List of x co-ords for ray-cell centre point
	DynamicList<scalar> xC;

	//List of y co-ords for ray-cell intersection point
	//First index corresponds to incoming point, second to outgoing
	DynamicList<scalar> yInF;
	DynamicList<scalar> yInB;

	//List of y co-ords for ray-cell centre point
	DynamicList<scalar> yC;

	//List of (2) ray counterpart labels
	List<label> counterpartRay;

	//List of (2) ray counterpart direction
	List<label> counterpartDirection;

	//List of (2) ray albedos
	List<scalar> alpha;
	
	//Azimuthal angle
	scalar azAngle;

	//Azimuthal angle index
	scalar azAngleIndex;

	//Total ray length
	scalar rayLength;

	//Starting coordinates
	point pStart;

	//Final coordinates
	point pEnd;

	//Azimuthal ray weight
	scalar wgta;

	//Azimuthal leakage factors - forward and reverse direction
	scalar leakF;
	scalar leakB;

	public:

	//Constructor
	rayDynamic(): counterpartRay(2), counterpartDirection(2), alpha(2){
		segments=0;
	}

	//Destructor
	~rayDynamic(){
		segmentLengths.clearStorage();
		cellIndices.clearStorage();
		xInF.clearStorage();
		yInF.clearStorage();
		xInB.clearStorage();
		yInB.clearStorage();
		xC.clearStorage();
		yC.clearStorage();
	}

	//Initialise ray with relevant values
	void initialise(point p0, point p1, scalar angle, 
	 label angleInd, scalar weight, label rayIndex)
	{
		pStart=p0;
		pEnd=p1;
		azAngle=angle;
		azAngleIndex=angleInd;
		wgta=weight;
		rayID=rayIndex;
	}

	//Set albedo values
	void setAlbedo(scalar a1, label dir) {
		alpha[dir]=a1;
	}

	//Set leakage factors
	void setLeakage(label dir, scalar cosine)
	{
		if(dir==0)
		{
			leakF=wgta*cosine;
		}else if(dir==1)
		{
			leakB=wgta*cosine;
		}else{Info<<"Error in setting leakage factors\n"<<endl;}
	}

	//Return leakage factor
	scalar returnLeakage(label dir)
	{
		if(dir==0){return leakF;}
		else if(dir==1){return leakB;}
		else {return EXIT_FAILURE;}
	}			

	//Increment number of segments, store segment length and cell ID
	void addSegment(scalar length, label cell) {
		++segments;
		segmentLengths.append(length);
		cellIndices.append(cell);
	}

	//Add new x entrant values
	void addXCoords(scalar xi, scalar xo) {
		xInF.append(xi);
		xInB.append(xo);
	}

	//Add new y entrant values
	void addYCoords(scalar yi, scalar yo) {
		yInF.append(yi);
		yInB.append(yo);
	}

	//Add new xC and yC values
	void addCCoords(scalar x, scalar y) {
		xC.append(x);
		yC.append(y);
	}

	//Store ray label of counterpart in chosen direction
	void addCounterpart(label ray1Direction, label compID, label compDirection) {
		counterpartRay[ray1Direction]=compID;
		counterpartDirection[ray1Direction]=compDirection;
	}

	//Return ray ID
	label returnRayID() {return rayID;};

	//Return counterpart ray ID
	label returnCounterpartRay(label ray1Direction) 
	{
		if((ray1Direction>1)||(ray1Direction<0))
		{
			Info<<"Input direction is not 0 or 1"<<endl;		
			return EXIT_FAILURE;
		}else{		
			return counterpartRay[ray1Direction];
		}
	}

	//Return counterpart ray direction
	label returnCounterpartDirection(label ray1Direction) 
	{
		if((ray1Direction>1)||(ray1Direction<0))
		{
			Info<<"Input direction is not 0 or 1"<<endl;		
			return EXIT_FAILURE;
		}else{			
			return counterpartDirection[ray1Direction];
		}
	}

	//Return ray albedo
	scalar returnAlbedo(label ray1Direction) 
	{
		if((ray1Direction>1)||(ray1Direction<0))
		{
			Info<<"Input direction is not 0 or 1"<<endl;		
			return EXIT_FAILURE;
		}else{	
			return alpha[ray1Direction];
		}
	}

	//Return number of segments
	label returnTotalSegments() {return segments;}

	//Return length of a segment
	scalar returnSegmentLength(label segID) 
	{
		if((segID>=segments)||(segID<0))
		{
			Info<<"Segment ID does not correspond to an existing index (either negative or greater than the maximum)"<<endl;		
			return EXIT_FAILURE;
		}else{		
			return segmentLengths[segID];	
		}
	}

	//Return segment cell ID
	label returnSegmentCellID(label segID) 
	{
		if((segID>=segments)||(segID<0))
		{
			Info<<"Segment ID does not correspond to an existing index (either negative or greater than the maximum)"<<endl;		
			return EXIT_FAILURE;
		}else{		
			return cellIndices[segID];
		}
	}

	//Return ray x entry point
	scalar returnXInCoord(label seg, label dir) {
		if(dir==0){ return xInF[seg];}
		else if (dir==1) {return xInB[seg];}
		else{return EXIT_FAILURE;}
	}

	//Return ray y entry point
	scalar returnYInCoord(label seg, label dir) {
		if(dir==0){ return yInF[seg];}
		else if (dir==1) {return yInB[seg];}
		else{return EXIT_FAILURE;}
	}

	//Return ray x centre point
	scalar returnXCCoord(label seg) {
		return xC[seg];
	}

	//Return ray y centre point
	scalar returnYCCoord(label seg) {
		return yC[seg];
	}

	//Return azimuthal angle
	scalar returnAngle() {return azAngle;}

	//Return azimuthal angle index
	label returnAngleIndex() {return azAngleIndex;}	

	//Return ray weight
	scalar returnRayWeight() {return wgta;}

	//Return ray start or end point
	point returnRayPoint(label direction)
	{
		if((direction>1)||(direction<0))
		{
			Info<<"Input direction is not 0 or 1"<<endl;		
			return pStart;	//lazy error handling
		}else if(direction==0)
		{
			return pStart;
		}else
		{
			return pEnd;
		}
	}

	//Method for error checking: ensure that ray segment lengths sum to give
	//total ray length
	label checkRayLength()
	{
		scalar sumSegments=0.0;
		for(label i=0; i<segments; i++)
		{
			sumSegments+=segmentLengths[i];
		}
		scalar checkLength;
		scalar rayLength=Foam::mag(pStart-pEnd);
		checkLength=Foam::mag(sumSegments-rayLength)/rayLength;
		if(checkLength<1e-7){return 1;}
		else{return 0;}
	}

	//Method for error checking: ensure no consecutively intersected cells are the same
	label checkCellOrder()
	{
		label cellID1, cellID2;
		cellID1=1;
		for(label i=0; i<segments; i++)
		{
			if(i==0){cellID1=cellIndices[i];}
			else{
				cellID2=cellID1;
				cellID1=cellIndices[i];
				if(cellID1==cellID2){return 0;}
			}
		}
		return 1;
	}
};


//Add method to return pointers to length list and cell list!!!


