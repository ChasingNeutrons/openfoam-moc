IOdictionary adjustArea
(
	IOobject
	(
		"adjustArea",
		runTime.constant(),
		mesh,
		IOobject::MUST_READ,
		IOobject::NO_WRITE
	)
);

PtrList<entry> adjustZone(adjustArea.lookup("zones"));

//Adjust area assuming the first entry of a region's dictionary is its outer radius
//while the second entry is its inner radius.
//Proceed to calculate the total area according to OpenFOAM and multiply each area in 
//this region by the ratio of true to approximate area.
forAll(adjustZone, zoneI)
{

	dictionary& dict = adjustZone[zoneI].dict();

	const word& name = adjustZone[zoneI].keyword();

	label zoneId = mesh.cellZones().findZoneID(name);
	if(zoneId == -1)
	{
	  Info << "Mesh zone: " << name << " does not exists" << endl;
	}

	//Accumulate OpenFOAM approximated area
	scalar foamArea=0.0;
	
	forAll(mesh.cellZones()[zoneId], cellIlocal)
	{
		label cellIglobal = mesh.cellZones()[zoneId][cellIlocal];
		foamArea+=area[cellIglobal];
	}

	//Read parameters and calculate true area assuming annular geometry
	scalar trueArea;
	scalar r1(readScalar(dict.lookup("r1")));
	scalar r2(readScalar(dict.lookup("r2")));
	scalar numZones(readScalar(dict.lookup("number")));
	
	foamArea/=numZones;
	
	if (r1>r2)
	{
		trueArea=PI*(r1*r1-r2*r2);
	}else{
		trueArea=PI*(r2*r2-r1*r1);
	}

	//Adjust area of each cell in the zone
	forAll(mesh.cellZones()[zoneId], cellIlocal)
	{
		label cellIglobal = mesh.cellZones()[zoneId][cellIlocal];
		area[cellIglobal]*=trueArea/foamArea;
	}
}
