volScalarField oldFissionSource=fissionSource[energyI];

fissionSource[energyI] *= 0.0;
forAll(flux,energyJ)
{
	fissionSource[energyI] += chi[energyI] * nuSigmaEff[energyJ] * flux[energyJ];
}
fissionSource[energyI]/=keff;
fissionSource[energyI]+=SORfac*(fissionSource[energyI] - oldFissionSource);
