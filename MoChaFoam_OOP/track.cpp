// track.cpp
#include "track.H"

track :: track()
{
	fBCTrack = NULL;
	bBCTrack = NULL;
}

void track :: initialise(label angleIdx, scalar angle, point start, point end)
{
	azIdx = angleIdx;
	phi = angle;
	p0 = start;
	p1 = end;
	fBC = -1;
	bBC = -1;
}

// Add another segment to the track
void track :: append(segment & seg)
{
	segments.push_back(seg);
}

// Set track ID
void setID(label id)
{
	ID = id;
}

// Set BC in given direction
void setBC(label dir, label bc)
{
	BC[dir] = bc;
}

// Set counterpart track (and its direction) in given direction (depending on BC)
void setCounterpart(track & t, label counterDir, label dir)
{
	counterTrack[dir] = t;
	counterBCDir[dir] = counterDir;
}

// Return the length of the i-th segment
scalar track :: getLength(label i)
{
	return segments[i]->getLength();
}

// Return the cell index of the i-th segment
label track :: getIdx(label i)
{
	return segments[i]->getIdx();
}

// Get an individual segment
segment* track :: getSegment(label i)
{
	return & segments[i];
}

// Get the segment vector
segment* track :: getSegment()
{
	return & segments[0];
}

// Return the number of segments composing the track
label track :: getNumSegments()
{
	return segments.size();
}

// Return the azimuthal angle of the track
scalar track :: getPhi()
{
	return phi;
}

// Return the boundary condition in givend directoon
label track :: getBC(label dir)
{
	return BC[dir];
}

// Return the counterpart boundary track in given direction
track* track :: getCounterpart(label dir)
{
	return & counterTrack[dir];
}

// Return the counterpart boundary track's direction in given direction
label track :: getCounterpartDir(label dir)
{
	return counterBCDir[dir];
}

// Destructor
track :: ~track()
{
	segments.clear();
	for(label i=0; i<2; i++)
	{
		counterTracks[i] = NULL;
	}
}
