//Found http://www.cfd-online.com/Forums/openfoam-programming-development/127449-ray-tracing-openfoam.html

//Set ray tracing options
treeBoundBox allBb(mesh.points());
   
scalar bbTol = 1e-12 * allBb.avgDim();
    
point& bbMin = allBb.min();
bbMin.x() -= bbTol;
bbMin.y() -= bbTol;
bbMin.z() -= bbTol;
    
point& bbMax = allBb.max();
bbMax.x() += 2*bbTol;
bbMax.y() += 2*bbTol;
bbMax.z() += 2*bbTol;
    
indexedOctree<treeDataFace> faceTree
(
	treeDataFace(false, mesh),       //formerly mesh_
	allBb, // overall search domain
	1, // maxLevel //formerly 50
	1, // leafsize //formerly 5
	1 // duplicity //formerly 3
);

label klines=0;	//track number of rays - index into storage for each ray
label failCount=0; //count how many rays failed to traverse the geometry

//Loop over all azimuthal angles
forAll(phi, i)							
{
	scalar t=tsi[i];	
	scalar p=phi[i];
	point pStart;
	point pBegin;
	point pEnd;
	int inc;
	scalar dx=t/mag(Foam::sin(p));
	scalar dy=t/mag(Foam::cos(p));
	vector dxvec(dx, 0.0, 0.0);
	vector dyvec(0.0, dy, 0.0);

	if(phi[i]<PI/2.0)
	{
		pStart=vector(0.0, 0.0, d_over_2);	//BOTTOM LEFT 	//point from which to begin all ray tracing
		inc=+1;					//direction in which rays are incremented on the x-axis
	}else
	{
		pStart=vector(w, 0.0, d_over_2);	//BOTTOM RIGHT 	//point from which to begin all ray tracing
		inc=-1;					//direction in which rays are incremented on the x-axis
	}
	
	label nnx=nx[i];
	label nny=ny[i];
	label nmax=nnx + nny;

	for (int n=0; n<nmax; n++)
	{		
		//Begin ray tracing from each starting point on x and y axis
		if(n<nnx)
		{
			pBegin=pStart + inc*(n+0.5)*dxvec; 	//plot along x-axis starting from origin +0.5*x-spacing
		}else
		{
			pBegin=pStart + (n-nnx+0.5)*dyvec;	//plot along y-axis starting from origin +0.5*y-spacing
		}
		
		scalar x0;
		if (p<PI/2.0){x0=w;}
		else{x0=0.0;}
		
		scalar ytest=Foam::tan(p)*(x0-pBegin.x()) + pBegin.y();
		
		if(ytest<h)
		{
			pEnd=vector(x0, ytest, d_over_2);
		}else
		{
			pEnd=vector( ((h-pBegin.y())/Foam::tan(p) + pBegin.x()), h, d_over_2);
		}
		
		//Define ray
		allTracks[klines].initialise(pBegin,pEnd,p,i,t*waz[i],klines);
		
		//Set boundary conditions based on start and endpoint
		//Also set leakage factor for each ray at start and end
		for (int r=0; r<2; r++)
		{
			//0 corresponds to start point, 1 to end point			
			point pRay=allTracks[klines].returnRayPoint(r);

			scalar yval=pRay.y();
			scalar xval=pRay.x();
			
			//Direction of ray hitting boundary - opposite to the r index
			int db;


			//Commented out setting leakage due to inconvenience when reading and writing rays
			//Can be returned straightforwardly but presently no purpose until acceleration added
			if(r==0){db=1;}
			else{db=0;}
			if(mag(xval)<1e-12) 
			{
				allTracks[klines].setAlbedo(alphaL,db);
				//allTracks[klines].setLeakage(db, mag(Foam::cos(p)));
			}
			else if(mag(xval-w)<1e-12) 
			{
				allTracks[klines].setAlbedo(alphaR,db);
				//allTracks[klines].setLeakage(db,mag(Foam::cos(p)) );
			}
			else if(mag(yval)<1e-12) 
			{
				allTracks[klines].setAlbedo(alphaB,db);
				//allTracks[klines].setLeakage(db,mag(Foam::sin(p)));
			}
			else if(mag(yval-h)<1e-12) 
			{
				allTracks[klines].setAlbedo(alphaT,db);
				//allTracks[klines].setLeakage(db,mag(Foam::sin(p)));
			}
			else {Info<<"Error in identifying boundary condition"<<endl;}
		}
	   
		vector eVec(pEnd - pBegin); // line vector
			    
		//const scalar eMag = mag(eVec); // edge length
		vector tolVec = 1e-12*eVec;    
		point p0 = pBegin + tolVec;
		point p1 = pEnd + tolVec;

		
		//int k=0;

		while(true)
		{
			pointIndexHit pHit = faceTree.findLine(p0, p1);
			if (pHit.hit())
			{
				//label faceI = pHit.index(); // face label of hit face - can be used to identify boundary!
				point hitPoint = pHit.hitPoint(); // intersection point

				label c=mesh.findCell(p0);
				scalar length=mag(hitPoint-p0);
				scalar xCentre=centres[c].component(0);
				scalar yCentre=centres[c].component(1);

				allTracks[klines].addSegment(length,c);
				approxArea[i][c]+=length*t;		//increment approximate cell area
				//approxArea[c]+=length*t*waz[i]/PI;		//increment approximate cell area
				
				//For linear source problems store local co-ordinates of cell entry point and ray centres
				//Must be wary of whether values are positive or negative!!				
				if(linearSource)
				{				
					allTracks[klines].addXCoords((p0.x()-xCentre),(hitPoint.x()-xCentre));
					allTracks[klines].addYCoords((p0.y()-yCentre),(hitPoint.y()-yCentre));
					allTracks[klines].addCCoords(((p0.x()+hitPoint.x())/2 - xCentre),((p0.y()+hitPoint.y())/2 - yCentre));
				}
				
				vector area = mesh.faceAreas()[pHit.index()];
				scalar typDim = Foam::sqrt(mag(area));
					
				// stop search if new start point is near to edge end
				if ((mag(hitPoint - pEnd)/typDim) < 1e-6)
				{
					klines++;
					break;
				}

				// set new start point shortly after previous start point
				p0 = hitPoint + tolVec;
				label newCell=mesh.findCell(p0);
				//Ensure p0 is not in the same cell!
				while(newCell==c){
					p0+=tolVec;
					newCell=mesh.findCell(p0);
				} 
			}
			else
			{	
				// No hit.
				scalar length=mag(p1-p0);
				label c=mesh.findCell(p0);
				allTracks[klines].addSegment(length,c);
				scalar xCentre=centres[c].component(0);
				scalar yCentre=centres[c].component(1);

				approxArea[i][c]+=length*t;		//increment approximate cell area
				//approxArea[c]+=length*t*waz[i]/PI;

				//For linear source problems store local co-ordinates of cell entry point
				if(linearSource) {
					allTracks[klines].addXCoords((p0.x()-xCentre),(p1.x()-xCentre));
					allTracks[klines].addYCoords((p0.y()-yCentre),(p1.y()-yCentre));
					allTracks[klines].addCCoords(((p0.x()+p1.x())/2 - xCentre),((p0.y()+p1.y())/2 - yCentre));
				}
				klines++;				
				Info<<"Ray tracing failure"<<endl;
				Info<<"Line "<<klines-1<<" Start point = "<<pBegin<<endl;
				Info<<"Angle ="<<p<<endl;
				Info<<"Failed ="<<p0<<endl;
				Info<<"End point ="<<pEnd<<endl;
				failCount++;
				break;
			}
		}
	}
}

if(failCount>0)
{
	Info<<failCount<<" rays of "<<totalRays<<" failed to trace."<<endl;
}
Info<<"Ray tracing completed with "<<totalRays<<" traced"<<endl;
