// expEval.cpp
#include "expEval.H"

// Constructor
expEval :: expEval(bool useInterp, trackArray & tracks, PtrList<volScalarField> & sigmaT);
{
	interpolate = useInterp;
	expTable = NULL;
	PtrList<volScalarField>* sig = sigmaT

	if(useInterp){
		scalar maxTau = tracks->getMaxOpticalLength(sig);
		expEval.constructTable(maxTau,tol);
	}
}

// Construct exponential lookup table
void expEval :: constructTable(scalar maxL, scalar tolerance)
{
	maxLength = maxL;
	label nArray = maxLength * sqrt(1.0/(8.0*tolerance));
	scalar spacing = maxLength / nArray;
	invSpacing = 1.0/spacing;

	// Increment to allow for tau = max optical length as
	// final value
	nArray++;

	expTable = new scalar[nArray];

	scalar tau;
	scalar expVal;
	scalar intercept;

	// Populate the table
	for(label i=0; i<nArray; i++){
		if(i==0){tau = 0.0;}
		else{tau = (i+0.5)*spacing;}

		expVal = Foam::exp(-tau);
		intercept = expVal * (1.0 + tau);
		expTable[i] = -expVal;
		expTable[i+1] = intercept;
	}

}

// Evaluate 1-exp(-tau)
scalar expEval :: evaluate(scalar tau)
{
	scalar val;

	if(interp){
		tau = Foam::min(tau, maxLength);
		label idx = Foam::floor(tau * invSpacing);
		val = 1.0 - (expTable[idx]*tau + expTable[idx+1]);
	}else{
		val = 1.0 - Foam::exp(-tau);
	}

    return val;
}

// Destructor
expEval :: ~expEval()
{
	delete [] expTable;
}
