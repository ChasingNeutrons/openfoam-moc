// trackArray.cpp
#include "trackArray.H"

// Initialise the track array
trackArray :: trackArray(quadrature & q)
{
	quad = q;
	totTracks = 0;
	nTracks = new label[q->getNumAz()];
	tracks = new track[q->getNumAz()];
	nAz_2 = q->getNumAz_2

	// Initialise track storage
	for(label i=0; i < nAz_2/2; i++){
		nTracks[i] = q->getNumX(i) + q->getNumY(i);
		nTracks[2*nAz_2 - i - 1] = nTracks[i];
		totTracks += 2*nTracks[i];
		tracks[i] = new track[nTracks[i]];
		tracks[nAz_2 - i - 1] = new track[nTracks[i]];
	}

	tracks1D = new track[totTracks];

}

// Ray trace to populate the track array
void trackArray :: rayTrace(mesh & transportMesh)
{

	mesh * m = transportMesh;
	// Identify bounds of the mesh
	treeBoundBox allBb(m->points());
	scalar bbTol = 1e-12 * allBb.avgDim();
	point& pMin = allBb.min();
	pMin.x() -= bbTol;
	pMin.y() -= bbTol;
	pMin.z() -= bbTol;
	point& pMax = allBb.max();
	pMax.x() += 2*bbTol;
	pMax.y() += 2*bbTol;
	pMax.z() += 2*bbTol;

	// Half depth of the geometry - ray trace from halfway up the z-axis
	scalar depth_2 = (pMin.z()+pMax.z())/2.0;

	// Loop over azimuthal angles
	for(i=0; i<nAz_2; i++){
		label nx = quad->getNumX(i);
		label ny = quad->getNumY(i);
		scalar tWidth = quad->getTrackSpacing(i);
		scalar phi = quad->getPhi(i);
		scalar dx = tWidth/mag(Foam::sin(phi));
		scalar dy = tWidth/mag(Foam::cos(phi));
		vector xVec = (dx, 0.0, 0.0);
		vector yVec = (0.0, dy, 0.0);

		point pStart;
		point p0;
		point pEnd;
		label inc;
		scalar x0;

		// Start either at the left or right x-boundary depending on angle
		if(phi < PI/2.0){
			pStart = vector(pMin.x(), pMin.y(), depth_2);
			inc = +1;
			x0 = pMax.x();
		}else{
			pStart = vector(pMax.x(), pMin.y(), depth_2);
			inc = -1;
			x0 = pMin.x();
		}

		// Create individual characteristic tracks
		for(j=0; j<(nx+ny); j++){

			if(j < nx){
				p0 = pStart + inc*(j+0.5)*xVec;
			}else{
				p0 = pStart + (j - nx + 0.5)*yVec; //removed inc here - should always be positive?
			}

			scalar yTest = Foam::tan(phi)*(x0-p0.x()) + p0.y();

			// Calculate where the ray intersects the geometry
			if(ytest<h){
				pEnd=vector(x0, ytest, depth_2);
			}else{
				pEnd=vector(((pMax.y()-p0.y())/Foam::tan(phi) + p0.x()), pMax.y(), depth_2);
			}

			// Initialise new track
			tracks[i][j].initialise(i,phi,p0,pEnd);

			// Initialise variables for ray tracing
			vector lineVec(pEnd - p0);
			lineVec /= mag(lineVec);
			vector tolVec = 1e-12*lineVec;

			// Perturb initial and final points to prevent errors
			p0 += tolVec;
			pEnd -= tolVec;
			point p = p0;

			// Locate initial cells
			label c0 = m->findCell(p0);
			label cEnd = m->findCell(pEnd);
			label c = c0;

			// Initialise mesh faces intersected
			label hitFace = 0;
			label entryFace = 0;

			// Identify which mesh boundary is behind the track
			//labelList Internal
			// Find boundary condition associated with boundary
			//label BC = 0;
			// Apply BC to track
			//allTracks[i][j].setBC(1,BC);

			// Begin ray tracing
			while(true){
				scalar segLength = 1000.0;
				bool pointHit = false;

				// From Pawel Kuczynski on CFD-Online.com
				const cell& faces = m->cells()[c];
				forAll(faces, faceI)
				{
					if(m->isInternalFace(faces[faceI]) && faces[faceI] != entryFace)
					{
						vector faceINormal = m->Sf()[faces[faceI]]/m->magSf()[faces[faceI]];
						const face& corners = m->faces()[faces[faceI]];
						label cornerID = corners[0];
						point faceCorner = pp[cornerID];

						scalar distanceDenominator = faceINormal.x()*lineVec.x()
								+ faceINormal.y()*lineVec.y();

						if(mag(distanceDenominator) > 0) //Zero if ray and surface are parallel
						{
							scalar distanceNumerator = faceINormal.x()*(faceCorner.x() - p.x())
									+ faceINormal.y()*(faceCorner.y()-p.y());
							scalar faceDistance = distanceNumerator/distanceDenominator;

							//Correct for node intersections
							if(mag(faceDistance - segmentLength) < 1e-10) //what should this tolerance be?
							{
								pointHit = true;
							}else if(faceDistance < segmentLength && faceDistance > 0)
							{
								segmentLength = faceDistance;
								hitFace = faces[faceI];
							}
						}
					}
				}

				if(entryFace == hitFace)
				{
					Foam::error<<"Ray tracing failure because hitFace problem of ray "<<j<<" angle "<<phi <<endl;
				}

				// Identify the next point from which to begin
				vector trans = segmentLength*lineVec;
				point hitPoint = p + trans;
				segment seg(segmentLength, c);


				//Add a new track segment
				allTracks[i][j].append(seg);

				// Check to see whether the end point is reached
				if(mag(p-pEnd)<1e-6)
				{
					// Identify which mesh boundary has been intersected
					//labelList Internal
					// Find boundary condition associated with boundary
					//label BC = 0;
					// Apply BC to track
					//allTracks[i][j].setBC(0,BC);
					break;
				}

				label newCell;
				// Here the new cell is ambiguous - must do a laborious search!
				if(pointHit)
				{
					label errorIterations = 1;
					newCell - m->findCell(hitPoint);
					while(newCell == c)
					{
						hitPoint += tolVec;
						newCell = mesh.findCell(hitPoint);
						errorIterations++;
						if(errorIterations>5){Foam::error<<"Could not find new cell"<<endl;}
					}
				}else if(c == mesh.neighbour()[hitFace])
				{
					newCell = mesh.owner()[hitFace];
				}else if(c == mesh.owner()[hitFace])
				{
					newCell = mesh.neighbour()[hitFace];
				}else
				{
					Foam::error<<"Could not identify new cell during ray tracing"<<endl;
				}

				c = newCell;
				p = hitPoint + tolVec;
				entryFace = hitFace;
			}
		}
	}

	construct1DArray();
}

// Get number of tracks for given azimuthal angle
label trackArray :: getNumTracksPerAngle(label azIdx)
{
	return nTracks[azIdx];
}

// Get total number of tracks
label trackArray :: getNumTracks()
{
	return totTracks;
}

// Get i-th track given angular index
track* trackArray :: getTrack(label azIdx, label i)
{
	return & tracks[azIdx][i];
}

// Get 2D array of tracks
track* trackArray :: get2DArray()
{
	return & tracks[0];
}

// Construct 1D array of tracks
void trackArray :: construct1DArray()
{
	id = 0;
	for(label i=0; i<nAz_2; i++)
	{
		for(label j=0; j<getNumTracksPerAngle(i); j++)
		{
			tracks[i][j].setID(id);
			tracks1D[id]=tracks[i][j];
			id++;
		}
	}
}

// Get 1D array of tracks
track* trackArray :: get1DArray()
{
	return & tracks1D[0];
}

// Find boundary that each ray intersects in both directions
// Identify the boundary condition in each direction
// If applicable, identify the counterpart ray
void trackArray :: setBoundaryConditions(){

	for(label i=0; i<nAz_2; i++){

		label nx = quad->getNumX(i);
		label ny = quad->getNumY(i);
		scalar phi = quad->getPhi(i);

		for(label j=0; j<(nx+ny); j++){

			// Forwards and backwards ray directions
			for(label dir=0; dir<2; dir++){

				if(tracks[i][j].getBC(dir) == -1){
					tracks[i][j].setBC(dir,BC);
					if((BC==1) || (BC==2)){
						tracks[i][j].setCounterpartRay();
					}
				}
			}
		}
	}
}

// Get maximum optical length for constructing exp table
scalar trackArray :: getMaxOpticalLength(PtrList<volScalarField>* sigmaT)
{
	segment* segs;
	segment* seg0;
	label nSeg;
	scalar maxLength = 0.0;
	scalar tau;

	// Find maximum optical length in the azimuthal plane
	for(label i=0; i<nAz_2; i++)
	{
		for(label j=0; j<getNumTracksPerAngle(i); j++)
		{
			segs = tracks[i][j].getSegments();
			nSeg = tracks[i][j].numSegments();
			for(label k=0; k<nSeg; k++)
			{
				seg = segs[k];
				tau = seg->getLength()*sigmaT[seg->getIdx()];
				maxLength = max(maxLength,tau);
			}
		}
	}

	// Find minimum sinTheta from quadrature
	scalar minSinTheta = 2.;
	scalar* sines = quad->getSinThetas();
	label npo = quad->getNPo();
	for(label i=0; i<nPo; i++)
	{
		if(sines[i] < minSinTheta)
			minSinTheta = sines[i];
	}

	return maxLength/minSinTheta;
}

// Destructor
trackArray :: ~trackArray()
{
	for(i=0; i < nAz_2; i++){
		delete [] tracks[i]
	}
	delete [] tracks;
	delete [] tracks1D;
	delete [] nTracks;
}
