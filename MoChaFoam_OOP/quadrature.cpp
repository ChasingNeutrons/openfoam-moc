// quadrature.cpp
#include "quadrature.H"

// Initialise the quadrature
quadrature :: quadrature(label numAz, label numPo, mesh & transportMesh, scalar d0, label polType)
{
	// Identify bounds of the mesh
	treeBoundBox allBb(transportMesh.points());
	scalar bbTol = 1e-12 * allBb.avgDim();
	point& pMin = allBb.min();
	pMin.x() -= bbTol;
	pMin.y() -= bbTol;
	pMin.z() -= bbTol;
	point& pMax = allBb.max();
	pMax.x() += 2*bbTol;
	pMax.y() += 2*bbTol;
	pMax.z() += 2*bbTol;

	scalar w = mag(pMax.x() - pMin.x());
	scalar h = mag(pMax.y() - pMin.y())

	if(numAz % 4 != 0){
		Info<<"Number of azimuthal angles must be divisible by 4"<<endl;
		Info<<"Adjusting number of azimuthal angles"<<endl;
		numAz += 4 - numAz % 4;
	}
	if(numAz < 1){
		Error<<"Number of azimuthal angles must be positive"<<endl;
	}

	wAz = NULL;
	phi = NULL;
	d = NULL;
	dInitial = d0;

	if(numPo < 1){
		Error<<"Number of polar angles must be positive"<<endl;
	}

	theta = NULL;
	sinTheta = NULL;

	if(w =< 0.0){
		Error<<"Geometry width must be positive"<<endl;
	}
	if(h =< 0.0){
		Error<<"Geometry height must be positive"<<endl;
	}

	width = w;
	height = h;

	polQuad = polType
	//TY quadrature
	if (polQuad == 1){
		constructTY();
	//GL quadrature
	}else if (polQuad == 2){
		constructGL();
    //Equal angle quadrature
	}else if (polQuad == 3){
		constructEA();
	//Error in polar input
	}else{
		Error<<"Invalid polar angle type selection"<<endl;
	}
	nAz = numAz;
	nAz_2 = numAz/2;

	// Obtain azimuthal quadrature
	calcAzimuthal();

	wTot = NULL;

	// Pre-calculate combined azimuthal and polar weights
	calcTotalWeight();

}

// Calculate the azimuthal quadrature given number of azimuthal angles and geometry
void quadrature :: calcAzimuthal()
{
	phi = new scalar(nAz_2);
	nx = new label(nAz_2);
	ny = new label(nAz_2);
	wAz = new scalar(nAz_2);
	d = new scalar(nAz_2);

	for(label i=0; i<nAz_2/2; i++)
	{
		scalar p=2*PI*(i+0.5)/nAz;

		label nnx = floor(width*mag(Foam::sin(p))/dInitial) + 1;
		label nny = floor(height*mag(Foam::cos(p))/dInitial) + 1;

		nx[i] = nnx;
		nx[nAz_2 - i - 1] = nnx;
		ny[i] = nny;
		ny[nAz_2 - i - 1] = nny;

		//Calculate effective angle
		p = Foam::atan((height*nnx)/(width*nny));
		phi[i] = p;
		phi[nAz_2 - i - 1] = PI - p;

		d[i]=w*mag(Foam::sin(p))/nnx;
		d[nAz_2 - i - 1] = d[i];

	}

	for(label i=0; i<nAz_2; i++){
		scalar w1, w2;
		if(i==0){
			w1 = phi[i];
			w2 = (phi[i+1]-phi[i])/2.0;
		}else if (i==nAz_2 - 1){
			w1 = (phi[i]-phi[i-1])/2.0;
			w2 = PI - phi[i];
		}else{
			w1 = (phi[i]-phi[i-1])/2.0;
			w2 = (phi[i+1]-phi[i])/2.0;
		}
		wAz[i]=w1+w2;
	}

}

// Construct the Tabuchi-Yamamoto quadrature
void quadrature :: constructTY()
{
	scalar* theta = new [] scalar(nPo);
	scalar* sinTheta = new [] scalar(nPo);
	scalar* wPo = new [] scalar(nPo);

	if (nPo == 1)
	{
		wPo[0]= 1.0;
		sinTheta[0]=0.798184;
	}else if (nPo == 2)
	{
		wPo[0]=0.212854;
		wPo[1]= 0.787146;
		sinTheta[0]=0.363900;
		sinTheta[1]= 0.899900;
	}else if (nPo == 3)
	{
		wPo[0]=0.046233;
		wPo[1]= 0.283619;
		wPo[2]= 0.670148;
		sinTheta[0]= 0.166648;
		sinTheta[1]= 0.537707;
		sinTheta[2]= 0.932954;
	}else{
		Error<<"Code does not support more than 3 polar angles for TY quadrature"<<endl;
	}

	for(label i; i<nPo; i++) {theta[i] = Foam::asin(sinTheta[i]);}
}

// Construct the Gauss-Legendre quadrature
void quadrature :: constructGL()
{
    scalar* theta = new [] scalar(nPo);
    scalar* sinTheta = new [] scalar(nPo);
    scalar* wPo = new [] scalar(nPo);

	if (nPo == 1) {
	    theta[0] = Foam::acos(0.5773502691)
	    wPo[0] = 1.0;
	}
	else if (nPo == 2) {
		theta[0] = Foam::acos(0.3399810435);
		theta[1] = Foam::acos(0.8611363115);
		wPo[0] = 0.6521451549;
		wPo[1] = 0.3478548451;
	}else if (nPo == 3) {
		theta[0] = Foam::acos(0.2386191860);
		theta[1] = Foam::acos(0.6612093864);
		theta[2] = Foam::acos(0.9324695142);
		wPo[0] = 0.4679139346;
		wPo[1] = 0.3607615730;
		wPo[2] = 0.1713244924;
	}else if (nPo == 4) {
		theta[0] = Foam::acos(0.1834346424);
		theta[1] = Foam::acos(0.5255324099);
		theta[2] = Foam::acos(0.7966664774);
		theta[3] = Foam::acos(0.9602898564);
		wPo[0] = 0.3626837834;
		wPo[1] = 0.3137066459;
		wPo[2] = 0.2223810344;
		wPo[3] = 0.1012285363;
	}else if (nPo == 5) {
		theta[0] = Foam::acos(0.1488743387);
		theta[1] = Foam::acos(0.4333953941);
		theta[2] = Foam::acos(0.6794095682);
		theta[3] = Foam::acos(0.8650633666);
		theta[4] = Foam::acos(0.9739065285);
		wPo[0] = 0.2955242247;
		wPo[1] = 0.2692667193;
		wPo[2] = 0.2190863625;
		wPo[3] = 0.1494513492;
		wPo[4] = 0.0666713443;
	}else if (nPo == 6) {
		theta[0] = Foam::acos(0.1252334085);
		theta[1] = Foam::acos(0.3678314989);
		theta[2] = Foam::acos(0.5873179542);
		theta[3] = Foam::acos(0.7699026741);
		theta[4] = Foam::acos(0.9041172563);
		theta[5] = Foam::acos(0.9815606342);
		wPo[0] = 0.2491470458;
		wPo[1] = 0.2334925365;
		wPo[2] = 0.2031674267;
		wPo[3] = 0.1600783286;
		wPo[4] = 0.1069393260;
		wPo[5] = 0.0471753364;
	}else{
		Error<<"Code does not support more than 6 polar angles for GL quadrature"<<endl;
	}

	for(label i; i<nPo; i++) {sinTheta[i] = Foam::sin(theta[i]);}
}

// Construct the Equal Angle polar quadrature
void quadrature :: constructEA()
{

}

// Pre-compute total quadrature weight
void quadrature :: calculateTotalWeight()
{
	wTot = new scalar(nAz_2);
	for(label i=0; i<nAz_2; i++){
		wTot[i] = new scalar(nPo);
		for(label j=0; j<nPo; j++){
			wTot[i][j] = 2.0*wAz[i]*d[i]*wPo[j]*sinTheta[j];
		}
	}
}

// Point to sinTheta array
scalar* quadrature :: getSinThetas()
{
	return & sinTheta[0];
}

// Return azimuthal angle
scalar quadrature :: getPhi(label azIdx)
{
	return phi[azIdx];
}

// Return track spacing
scalar quadrature :: getTrackSpacing(label azIdx)
{
	return d[azIdx];
}

// Return number of azimuthal angles
label quadrature :: getNumAz()
{
	return nAz;
}

// Return number of polar angles
label quadrature :: getNumPo()
{
	return nPo;
}

// Return half number of azimuthal angles
label quadrature :: getNumAz_2()
{
	return nAz_2;
}

// Return number of tracks along x boundary
label quadrature :: getNumX(label azIdx)
{
	return nx[azIdx];
}

// Return number of tracks along y boundary
label quadrature :: getNumY(label azIdx)
{
	return ny[azIdx];
}

// Return azimuthal weight
scalar quadrature :: getWAz(label i)
{
	return wAz[i];
}

// Return azimuthal width
scalar quadrature :: getWidthAz(label i)
{
	return d[i];
}

// Return polar weight
scalar quadrature :: getWPo(label i)
{
	return wPo[i];
}

// Return total weight
scalar quadrature :: getWeight(label azIdx, label polIdx)
{
	return wTot[azIdx][polIdx];
}

// Destructor
quadrature :: ~quadrature()
{
	for(i=0; i < nAz_2; i++){
		delete [] wTot[i]
	}
	delete [] wTot;
	delete [] wAz;
	delete [] wPo;
	delete [] d;
	delete [] phi;
	delete [] cosPhi;
	delete [] theta;
	delete [] sinTheta;
	delete [] nx;
	delete [] ny;
}
